import tensorflow as tf
import numpy as np
import pickle
from sklearn import preprocessing
from tensorflow.keras import layers


def predict_lang(model, text: str):
    lang_list = ["es", "en", "de"]

    le = preprocessing.LabelEncoder()
    le.fit(lang_list)

    from_disk = pickle.load(open("saved_model/layer.pkl", "rb"))
    new_v = layers.TextVectorization.from_config(from_disk['config'])
    new_v.adapt(tf.data.Dataset.from_tensor_slices(["xyz"]))
    new_v.set_weights(from_disk['weights'])

    examples_vectorized = new_v([text])

    logits = model.predict(examples_vectorized)
    probits = tf.nn.softmax(logits)
    idx_predictions = np.argmax(probits, axis=1)

    return(le.inverse_transform(idx_predictions), np.max(probits, axis=1))