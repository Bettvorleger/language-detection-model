from db_functions import view_all
from flask import Flask
from flask import jsonify
from db_functions import view_all

app = Flask(__name__)


@app.route('/getFeedback')
def example():
    return jsonify(view_all())


if __name__ == '__main__':
    app.run()
