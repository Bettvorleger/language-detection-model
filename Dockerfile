FROM python:3.9
EXPOSE 8501
EXPOSE 5000
WORKDIR /app
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt
RUN export FLASK_APP=db_results.py
RUN export FLASK_ENV=development
COPY . .
CMD streamlit run app.py --server.port $PORT && flask run --port=$PORT
