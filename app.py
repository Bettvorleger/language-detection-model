import streamlit as st
import time
import tensorflow as tf

from db_functions import *
from predict import *


@st.cache(allow_output_mutation=True)
def load_model(path: str) -> str:
    if path == '':
        return None
    else:
        return tf.keras.models.load_model(path)


def _detect_language(model, text: str):
    lang_vector = {"en": "English",
                   "de": "German",
                   "es": "Spanish"}
    language = "Gibberish"
    (lang, prob) = predict_lang(model, text)
    if (prob > 0.40):
        language = lang_vector[lang[0]]
    st.write("### This text appears to be the language: {}.".format(language))
    st.write("###### (with a probability of: {})".format(prob))
    st.write("## ##")


if __name__ == "__main__":

    create_table()
    if 'show_feedback' not in st.session_state:
        st.session_state.show_feedback = False

    language_model = load_model(path="saved_model/simple_mlp_novectorize.h5")
    with st.sidebar:
        """
        This is a simple streamlit application that guesses the language of the text passed.
        """

    text_input = st.text_area(
        "Please enter sample text to detect language: ",
        value="",
        placeholder=None,
        disabled=False,
    )

    button = st.button(
        "Detect language",
        on_click=_detect_language,
        args=(
            language_model,
            text_input,
        ),
    )

    if button:
        st.session_state.show_feedback = True

    if st.session_state.show_feedback:

        st.markdown("## ##")
        st.write("Prediction not correct?")

        choice = st.selectbox("Choose correct language",
                              ("German", "English", "Spanish"))
        if st.button("Submit feedback"):
            add_feedback(text_input, choice)
            st.success("Thank you! Your feedback was submitted")
            st.session_state.show_feedback = False
            time.sleep(2)
            st.experimental_rerun()

    with st.expander("Show DB entries"):
        st.write(view_all())
