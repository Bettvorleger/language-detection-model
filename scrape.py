import requests
import pandas as pd
from bs4 import BeautifulSoup
import re
import csv
import os
from tqdm import tqdm


def createUrlList():
    if os.path.exists("wiki_urls.txt"):
        os.remove("wiki_urls.txt")

    S = requests.Session()

    articles = {"en": "United_States",
                "de": "Deutschland",
                "es": "España"}

    for lang in articles:

        api = "https://"+lang+".wikipedia.org/w/api.php"

        PARAMS = {
            "generator": "links",
            "action": "query",
            "format": "json",
            "titles": articles[lang],
            "prop": "info",
            "inprop": "url",
            "gpllimit": 320 if lang == 'es' else (180 if lang == 'en' else 170)
        }

        R = S.get(url=api, params=PARAMS)
        data = R.json()

        pages = data["query"]["pages"]
        f = open("wiki_urls.txt", "a")

        for k, v in tqdm(pages.items(), desc="Saving wiki urls ("+lang+")"):
            f.write(v["canonicalurl"]+"\n")

        f.close()


def splitFile():
    df = pd.read_csv('scrape.csv')

    df = df.sample(frac=1)

    train = df.sample(frac=0.8)
    rest = df.loc[~df.index.isin(train.index)]

    test = rest.sample(frac=0.5)
    val = rest.loc[~rest.index.isin(test.index)]

    train.to_csv('data/scrape_train.csv', index=False)
    test.to_csv('data/scrape_test.csv', index=False)
    val.to_csv('data/scrape_val.csv', index=False)


def scrapeList():

    if os.path.exists("scrape.csv"):
        os.remove("scrape.csv")

    createUrlList()

    csv.writer(open('scrape.csv', 'w')).writerow(['labels', 'text'])

    urls = open('wiki_urls.txt', 'r')
    for url in tqdm(urls, desc="Saving text to csv", total=len(open('wiki_urls.txt').readlines())):

        address = url.replace('\n', '')
        host = address.partition('://')[2]
        lang = host.partition('.')[0]

        response = requests.get(address)

        soup = BeautifulSoup(response.content, 'lxml')

        text = ''
        for paragraph in soup.find_all('p'):
            text += paragraph.text

        text = re.sub(r'\[.*?\]+', '', text)
        text = text.replace('\n', ' ')
        text = re.sub("[\(\[].*?[\)\]]", "", text)

        sentences = re.split(
            r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', text)

        file = csv.writer(open('scrape.csv', 'a'))
        for s in sentences:
            s = re.sub('[^a-zA-Z0-9 \n\.\?\!]', '', s)
            if len(s.split()) > 200:
                file.writerow([lang, ' '.join(s.split()[: 200])])
            elif len(s.split()) > 2:
                file.writerow([lang, s])

    splitFile()
