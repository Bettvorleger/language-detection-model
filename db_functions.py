import sqlite3

conn = sqlite3.connect('data.db', check_same_thread=False)
c = conn.cursor()

def create_table():
    c.execute('CREATE TABLE IF NOT EXISTS feedback(input TEXT, language TEXT)')

def add_feedback(input, language):
    c.execute('INSERT INTO feedback(input, language) VALUES(?,?)', (input, language))
    conn.commit()

def view_all():
    c.execute('SELECT * FROM feedback')
    data = c.fetchall()
    return data