import argparse
import tensorflow as tf
import pandas as pd
import pickle
from sklearn import preprocessing

from tensorflow.keras import layers
from tensorflow.keras import losses

from scrape import scrapeList

parser = argparse.ArgumentParser()
parser.add_argument("-s", action='store_true',
                    help="Train without scraping first")
args = parser.parse_args()

if args.s:
    print("Scraping the web for text input data...")
    scrapeList()

DATA_DIRECTORY = "data"

train_df = pd.read_csv(f"{DATA_DIRECTORY}/scrape_train.csv")
val_df = pd.read_csv(f"{DATA_DIRECTORY}/scrape_val.csv")
test_df = pd.read_csv(f"{DATA_DIRECTORY}/scrape_test.csv")

nan_value = float("NaN")
train_df.replace("", nan_value, inplace=True)
val_df.replace("", nan_value, inplace=True)
test_df.replace("", nan_value, inplace=True)

train_df.dropna(how='all', axis=1, inplace=True)
val_df.dropna(how='all', axis=1, inplace=True)
test_df.dropna(how='all', axis=1, inplace=True)

lang_list = ["es", "en", "de"]

le = preprocessing.LabelEncoder()
le.fit(lang_list)

num_classes = len(le.classes_)

train_labels = tf.keras.utils.to_categorical(
    le.transform(train_df.pop('labels')), num_classes=num_classes)
val_labels = tf.keras.utils.to_categorical(
    le.transform(val_df.pop('labels')), num_classes=num_classes)
test_labels = tf.keras.utils.to_categorical(
    le.transform(test_df.pop('labels')), num_classes=num_classes)

raw_train_ds = tf.data.Dataset.from_tensor_slices(
    (train_df["text"].to_list(), train_labels))  # X, y
raw_val_ds = tf.data.Dataset.from_tensor_slices(
    (val_df["text"].to_list(), val_labels))
raw_test_ds = tf.data.Dataset.from_tensor_slices(
    (test_df["text"].to_list(), test_labels))

batch_size = 32
max_features = 12000  # top 10K most frequent words
sequence_length = 45  # We defined it in the previous data exploration section

vectorize_layer = layers.TextVectorization(
    standardize="lower_and_strip_punctuation",
    max_tokens=max_features,
    output_mode='int',
    output_sequence_length=sequence_length)

vectorize_layer.adapt(train_df["text"].to_list())

# returns vectorize_layer(text), label
train_ds = raw_train_ds.map(lambda x, y: (vectorize_layer(x), y))
val_ds = raw_val_ds.map(lambda x, y: (vectorize_layer(x), y))
test_ds = raw_test_ds.map(lambda x, y: (vectorize_layer(x), y))


AUTOTUNE = tf.data.AUTOTUNE

train_ds = train_ds.batch(batch_size=batch_size)
train_ds = train_ds.prefetch(AUTOTUNE)
val_ds = val_ds.batch(batch_size=batch_size)
val_ds = val_ds.prefetch(AUTOTUNE)
test_ds = test_ds.batch(batch_size=batch_size)
test_ds = test_ds.prefetch(AUTOTUNE)

embedding_dim = 16
model = tf.keras.Sequential([
    layers.Embedding(max_features + 1, embedding_dim),
    layers.Dropout(0.2),
    layers.GlobalAveragePooling1D(),
    layers.Dropout(0.2),
    layers.Dense(3)])

model.compile(loss=losses.CategoricalCrossentropy(from_logits=True, label_smoothing=0.1),
              optimizer='adam',
              metrics=['accuracy'])

epochs = 10
history = model.fit(
    train_ds,
    validation_data=val_ds,
    epochs=epochs)

loss, accuracy = model.evaluate(test_ds)

print("Saving model and config data in folder: saved_model/")
model.save('saved_model/simple_mlp_novectorize.h5')

pickle.dump({'config': vectorize_layer.get_config(),
             'weights': vectorize_layer.get_weights()}, open("saved_model/layer.pkl", "wb"))
